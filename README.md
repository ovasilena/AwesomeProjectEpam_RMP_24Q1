# AwesomeProjectEpam_RMP_24Q1

This is the new [React Native](https://reactnative.dev) project, initialized using [`@react-native-community/cli`](https://github.com/react-native-community/cli). This [homework task](https://d17btkcdsmqrmh.cloudfront.net/react-native/docs/module-1-react-native-fundamentals/module-1-homework) is aimed at setting up the basic environment and installing the tools necessary for debugging a React Native application.

## Description

It was created by using the command:
```
npx react-native@latest init AwesomeProjectEpam_RMP_24Q1
```

## Getting Started

> Note: Make sure you have completed the [React Native - Environment Setup](https://reactnative.dev/docs/environment-setup) instructions before proceeding.

## Step 1: Start the Metro Server

In order to start [Metro](https://reactnative.dev/docs/metro), the JavaScript bundler that ships with React Native, run the following command from the root of your React Native project:

```bash
# using npm
npm start

# OR using Yarn
yarn start
```

## Step 2: Start your Application

With Metro Bundler running, start your application on either Android or iOS:

### For Android

```bash
# using npm
npm run android

# OR using Yarn
yarn android
```

### For iOS

```bash
# using npm
npm run ios

# OR using Yarn
yarn ios
```

If setup correctly, you should shortly see your new app running on Android Emulator or iOS Simulator.

## Step 3: Modifying your App

Open `App.tsx` in your text editor of choice and edit some lines. To see your changes, press the R key twice or select "Reload" from the Developer Menu for Android or use Cmd + R in your iOS Simulator.

## Congratulations! 🥳

You've successfully run and modified your React Native App.

## Troubleshooting
If you encounter any issues, check the [Troubleshooting](https://reactnative.dev/docs/troubleshooting) page.

## Learn More
For additional resources on React Native:

- EPAM React Native mentoring [BOOK](https://d17btkcdsmqrmh.cloudfront.net/react-native/docs/intro)
- Design [figma](https://www.figma.com/file/N6hgISN3shngz7akdCz7Sj/Easy-App-(React-Native-Course)?type=design&node-id=41-1019&mode=design&t=A8r17ia1ZkMFXqpM-0)
- [React Native Website](https://reactnative.dev)
- [Getting Started](https://reactnative.dev/docs/environment-setup)
- [Learn the Basics](https://reactnative.dev/docs/getting-started)
- [React Native Blog](https://reactnative.dev/blog)
- [React Native on GitHub](https://github.com/facebook/react-native)

