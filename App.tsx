import React from 'react';
import { SafeAreaView, StatusBar, Text, View, StyleSheet } from 'react-native';

function App() {
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <View style={styles.header}>
        <Text style={styles.headerText}>Awesome App</Text>
      </View>
      <View style={styles.content}>
        <Text style={styles.title}>Welcome to My App!</Text>
        <Text style={styles.description}>
          This is the start screen of my new awesome app. Let's rock this journey together!
        </Text>
      </View>
      <View style={styles.footer}>
        <Text style={styles.footerText}>Let's start the adventure!</Text>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 40,
  },
  header: {
    padding: 20,
    backgroundColor: '#ff6347',
  },
  headerText: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#fff',
    textAlign: 'center',
  },
  content: {
    flex: 1,
    padding: 16,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffebcd',
  },
  title: {
    fontSize: 28,
    fontWeight: 'bold',
    color: '#008080',
    marginBottom: 16,
    textAlign: 'center',
  },
  description: {
    fontSize: 18,
    lineHeight: 28,
    color: '#008080',
    textAlign: 'center',
  },
  footer: {
    padding: 20,
    backgroundColor: '#ff6347',
  },
  footerText: {
    fontSize: 18,
    color: '#fff',
    textAlign: 'center',
  },
});
export default App;
